package fer.ruazosa.stoperica_usluge

import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    var stop: Boolean = false
    var pause: Boolean = false
    var start: Boolean = true
    var counter: Int = 0

    private lateinit var sleepService: SleepService
    private var bound: Boolean = false

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as SleepService.SleepServiceBinder
            sleepService = binder.service
            bound = true
            Toast.makeText(this@MainActivity, "Service connected", Toast.LENGTH_LONG).show()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            bound = false
            Toast.makeText(this@MainActivity, "Service disconnected", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton.setOnClickListener {
            if (bound) {
                stop = false
                Thread {
                    if (start == true) {
                        counter = 0
                        start = false
                        while (true) {
                            if (stop == true) {
                                break
                            }
                            if (pause == false) {
                                runOnUiThread {
                                    timerTextView.text = counter.toString()
                                    counter++
                                }
                            }
                            sleepService.sleep(1)
                        }
                    }
                }.start()
            }
        }

        pauseButton.setOnClickListener {
            if (stop == false) {
                if (pause == true) {
                    pause = false
                    pauseButton.text = "PAUSE"
                    val toastMessage = Toast.makeText(applicationContext, "CONTINUED", Toast.LENGTH_SHORT)
                    toastMessage.show()
                }
                else {
                    pause = true
                    val toastMessage = Toast.makeText(applicationContext, "PAUSED", Toast.LENGTH_SHORT)
                    toastMessage.show()
                    pauseButton.text = "CONTINUE"
                }
            }
        }

        stopButton.setOnClickListener {
            stop = true
            start = true
            pause = false
            pauseButton.text = "PAUSE"
            val toastMessage = Toast.makeText(applicationContext, "STOPPED", Toast.LENGTH_LONG)
            toastMessage.show()
        }

    }

    override fun onStart() {
        super.onStart()
        // Bind to LocalService
        Intent(this, SleepService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
    }
}