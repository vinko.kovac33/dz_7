package fer.ruazosa.stoperica_usluge

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder


class SleepService : Service() {

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    val binder: IBinder = SleepServiceBinder()

    inner class SleepServiceBinder : Binder(){
        val service: SleepService = this@SleepService
    }

    fun sleep(secondsToSleep: Int) {
        Thread.sleep(secondsToSleep.toLong()*1000)
    }

}